const questions = [
    "In what year was the copyright law created?",
    "What is meant with intellectual property?",
    "Who is obligated to include legal notice?",
    "What needs to be included in the legal notice?",
    "What is important about pictures of people?",
    "When are you allowed to use/mention trademarks on your page?",
    "What cannot appear in a domain name?",
    "How can you use advertisement on your page?",
    "Can the webpage operator be held accountable for third-party ads displayed on their site?",
    "Is oral permission enough?",
    "What is fair use?",
    "The Public Domain consists of works without property rights",
    "Licences..."
];

const answers = [
    ["1992", "1990", "2001"],
    ["Immaterial goods", "Physical things", "Only books"],
    ["Commercial websites", "Everyone", "Governmental websites"],
    ["Full name, place of residence, email address", "Full name, place of residence, telephone number", "Full name, birth date, place of residence"],
    ["All people appearing in the image need to consent to its publication", "No consent is needed", "If the image has a lesser resolution than 256x256, no consent is required"],
    ["Trademarks of services and products sold on the page", "Any trademarked names", "Never without proper permission"],
    ["Trademarks, names of cities, popular people", "Single words", "Your last name"],
    ["Ads need to be clearly distinct from page content", "Ads can appear anywhere", "In a way that the user won't notice it"],
    ["Yes", "No", "Not when the blame is rejected in the imprint"],
    ["It's always better to have it written down", "Depends on the context", "Only during full moons"],
    ["The conditions under which copyrighted work can be used", "It's a law invented in 1999 to prevent misuse of copyrighted works", "It's an effort protecting user's privacy"],
    ["Correct", "Wrong", "I should've learned more for this test"],
    ["Empower the licence holder to use something owned by someone else", "Disallow a person, to which the licence is directed, usage of the IP", "Taste well with some salt"]
];

class Quiz {
    #_questions;
    #_answers;
    #_question;
    #_answer1;
    #_answer2;
    #_answer3;
    #_questionIndex;
    #_userAnswers;
    #_selectedAnswer;
    #_btnNext;
    #_correctAnswers;
    #_answerIndexes;

    constructor() {
        this.#_questions = questions;
        this.#_answers = answers;
        this.#_question = document.getElementById('question');
        this.#_answer1 = document.getElementById('a1');
        this.#_answer2 = document.getElementById('a2');
        this.#_answer3 = document.getElementById('a3');
        this.#_questionIndex = 0;
        this.#_userAnswers = [];
        this.#_btnNext = document.getElementById('btnNext');
        this.#_correctAnswers = 0;
        this.#initLabels();
        this.#initButtons();
        this.#shuffleQuestions();
        this.#loadQuestion();
    }

    #initLabels() {
        const labels = document.getElementsByTagName('label');

        for (const label of labels) {
            if (label.htmlFor != '') {
                const element = document.getElementById(label.htmlFor);
                if (element) {
                    element.label = label;
                }
            }
        }
    }

    #initButtons() {
        this.#_answer1.parentNode.onclick = () => {
            this.selectAnswer(0);
            this.#_answer1.checked = true;
        };

        this.#_answer2.parentNode.onclick = () => {
            this.selectAnswer(1);
            this.#_answer2.checked = true;
        };

        this.#_answer3.parentNode.onclick = () => {
            this.selectAnswer(2);
            this.#_answer3.checked = true;
        };

        this.#_btnNext.disabled = true;
        this.#_btnNext.onclick = () => {
            this.sendAnswer();
            this.#_btnNext.disabled = true;
        };
    }

    // Fisher-Yates (Knuth) Shuffle.
    #shuffleQuestions() {
        let currentIndex = this.#_questions.length;
        let randomIndex = 0;

        while (currentIndex != 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            const tmpQuestion = this.#_questions[currentIndex];
            this.#_questions[currentIndex] = this.#_questions[randomIndex];
            this.#_questions[randomIndex] = tmpQuestion;

            const tmpAnswer = this.#_answers[currentIndex];
            this.#_answers[currentIndex] = this.#_answers[randomIndex];
            this.#_answers[randomIndex] = tmpAnswer;
        }
    }

    #generateRandomAnswerIndexes() {
        let arr = [0, 1, 2];
        let currentIndex = arr.length;
        let randomIndex = 0;

        while (currentIndex != 0) {
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex--;

            [arr[currentIndex], arr[randomIndex]] = [
                arr[randomIndex], arr[currentIndex]
            ];
        }

        return arr;
    }

    #storeAnswer() {
        this.#_userAnswers[this.#_questionIndex] = this.#_selectedAnswer;

        if (this.#_answerIndexes[this.#_selectedAnswer] === 0) {
            this.#_correctAnswers++;
        }
    }

    #advanceIndex() {
        this.#_questionIndex++;
        return this.#_questionIndex < this.#_questions.length;
    }

    #loadQuestion() {
        this.#_question.innerText = this.#_questions[this.#_questionIndex];

        // Fancy, but it works... ?
        // Reasoning: the first answer of the array is always the right answer.
        // If we would shuffle the answer arrays, this wouldn't be true anymore.
        // So instead, we shuffle indexes...
        this.#_answerIndexes = this.#generateRandomAnswerIndexes();
        this.#_answer1.label.innerText = this.#_answers[this.#_questionIndex][this.#_answerIndexes[0]];
        this.#_answer2.label.innerText = this.#_answers[this.#_questionIndex][this.#_answerIndexes[1]];
        this.#_answer3.label.innerText = this.#_answers[this.#_questionIndex][this.#_answerIndexes[2]];

        this.#_answer1.checked = false;
        this.#_answer2.checked = false;
        this.#_answer3.checked = false;

        this.#_selectedAnswer = null;
    }

    selectAnswer(idx) {
        this.#_selectedAnswer = idx;
        this.#_btnNext.disabled = false;
    }

    sendAnswer() {
        if (this.#_selectedAnswer === null) {
            return;
        }

        this.#storeAnswer();

        if (this.#advanceIndex()) {
            this.#loadQuestion();
        } else {
            window.location.replace("result.html?c=" + this.#_correctAnswers);
        }
    }

    getAnswers() {
        return this.#_userAnswers;
    }

    registerKeys() {
        const that = this;
        window.addEventListener("keydown", function(e) {
            const c = String.fromCharCode(e.which).toLowerCase();
            switch (c) {
                case "a":
                    that.selectAnswer(0);
                    that.#_answer1.checked = true;
                    break;
                case "b":
                    that.selectAnswer(1);
                    that.#_answer2.checked = true;
                    break;
                case "c":
                    that.selectAnswer(2);
                    that.#_answer3.checked = true;
                    break;
                case "n":
                    that.sendAnswer();
                    that.#_btnNext.disabled = true;
                    break;
            }
        });
    }
}

const quiz = new Quiz();
quiz.registerKeys();
