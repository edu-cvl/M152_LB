function consent() {
    return getCookie("accepted");
}

function updateVisited() {
    const path = window.location.pathname;
    const cookie = getCookie("visited");

    let body = [];
    if (cookie) {
        body = JSON.parse(cookie[1]);
    }

    for (let b of body) {
        if (b.name === path) {
            // Page was visited before, increase the counter.
            b.count++;

            const payload = JSON.stringify(body);
            setCookie("visited", payload, 31);

            return;
        }
    }

    // Page wasn't visited before, create counter.
    body.push({
        name: path,
        count: 1,
        time: 0
    });

    const payload = JSON.stringify(body);
    setCookie("visited", payload, 31);
}

let timeElapsed = 0;
let startDate = new Date();

function startTimer() {
    startDate = new Date();
}

function stopTimer() {
    const endDate = new Date();
    const spentTime = endDate.getTime() - startDate.getTime();
    timeElapsed += spentTime;
}

function updateTimeSpent() {
    const path = window.location.pathname;
    const cookie = getCookie("visited");
    //const timeSpent = performance.now();

    let body = [];
    if (cookie) {
        body = JSON.parse(cookie[1]);
    }

    for (let b of body) {
        if (b.name === path) {
            // Page was visited before, add time.
            b.time += timeElapsed;

            const payload = JSON.stringify(body);
            setCookie("visited", payload, 31);

            return;
        }
    }

    // Page wasn't visited before, create it.
    body.push({
        name: path,
        count: 1,
        time: timeElapsed
    });

    const payload = JSON.stringify(body);
    setCookie("visited", payload, 31);
}

function setCookie(name, value, days) {
    const date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));

    const expires = "; expires=" + date.toUTCString();

    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(name) {
    var cookies = document.cookie.split("; ");
    for (let c of cookies) {
        c = c.split('=');

        if (c[0] === name) {
            return c;
        }
    }
}

if (consent()) {
    updateVisited();

    window.onbeforeunload = () => {
        stopTimer();
        updateTimeSpent();
    };

    window.onblur = () => {
        stopTimer();
    };

    window.onfocus = () => {
        startTimer();
    };
}
