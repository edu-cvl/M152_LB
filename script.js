function updateAsides() {
    // Exit if side-notes are inlined.
    if (document.body.clientWidth < 800) return;

    document.querySelectorAll('aside').forEach((aside) => {
        // Each aside should be positioned on the height of an accompanying
        // span.
        const name = aside.getAttribute('name');
        const span = document.querySelector("span[name='" + name + "']");

        if (null == span) {
            console.log("Could not find span for '" + name + "'");
        }

        // Set the height of the aside.
        // `getBoundingClientRect` returns us the distance relative to the
        // viewport (currently visible part of the document). In order to get
        // the position to the real top of the document we need to add
        // `scrollY` to it.
        aside.style.top = span.getBoundingClientRect().top - 3 + window.scrollY +
            "px";
    });
}

// Invokes `callback` once all images have been loaded.
function afterImagesLoaded(callback) {
    const imgs = document.images;
    const len = imgs.length;
    let counter = 0;

    const incrementCounter = () => {
        counter++;

        if (counter === len) {
            callback();
        }
    };

    [].forEach.call(imgs, function(img) {
        if (img.complete) {
            incrementCounter();
        } else {
            img.addEventListener('load', incrementCounter, false);
        }
    });
}

// Invokes `callback` once all fonts have been loaded.
function afterFontsLoaded(callback) {
    document.fonts.ready.then(callback);
}

// Re-calculate asides top position when window is resized.
window.onresize = () => {
    updateAsides();
};


updateAsides();
afterImagesLoaded(updateAsides);
afterFontsLoaded(updateAsides);



/* Keyboard Shortcuts */
const linkNext = document.getElementById("link-next");
const linkPrev = document.getElementById("link-prev");

window.addEventListener("keydown", function(e) {
    const c = String.fromCharCode(e.which).toLowerCase();

    if (linkNext && c === "n") {
        linkNext.click();
    } else if (linkPrev && c === "p") {
        linkPrev.click();
    } else if (c === "h") {
        window.location.replace("/");
    }
});
