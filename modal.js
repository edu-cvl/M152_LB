const modal = document.getElementById("modal");
const modalImage = document.getElementById("modal-image");
const modalCaption = document.getElementById("modal-caption");
const modalClose = document.getElementById("modal-close");

function closeModal() {
    modal.style.display = "none";
}

modalClose.onclick = closeModal;

modal.addEventListener("click", closeModal);

document.addEventListener("keyup", function(e) {
    e = e || window.event;
    if (e.key == "Escape") {
        closeModal();
    }
});

const main = document.getElementsByTagName("main")[0];
const allImages = main.getElementsByTagName("img");

modalImage.addEventListener("click", function(e) {
    e.stopPropagation();
});
modalCaption.addEventListener("click", function(e) {
    e.stopPropagation();
});

function registerImage(img) {
    img.onclick = function() {
        modal.style.display = "block";
        modalImage.src = this.src;
        modalImage.title = this.title;
        modalCaption.innerHTML = this.title;
    };
}

function registerAllInMain() {
    for (let i = 0; i < allImages.length; ++i) {
        const cImg = allImages[i];
        cImg.onclick = function() {
            modal.style.display = "block";
            modalImage.src = this.src;
            modalImage.title = this.title;
            modalCaption.innerHTML = this.title;
        };
    }
}
