function initializeCollapsibles() {
    const collapsibles = document.querySelectorAll(".collapsible");

    collapsibles.forEach((coll) => {
        coll.addEventListener("click", function() {
            this.classList.toggle("active");
            const content = this.nextElementSibling;
            if (content.style.maxHeight) {
                content.style.maxHeight = null;
            } else {
                content.style.maxHeight = content.scrollHeight + "px";
            }

            if (typeof calculateProgress === "function") {
                calculateProgress();
            }
        });
    });
}

initializeCollapsibles();
