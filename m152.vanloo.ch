map $sent_http_content_type $expires {
    default                    off;
    text/css                   30d;
    application/javascript     15m;
    ~image/                    30d;
}

server {

	server_name m152.vanloo.ch;

	root /var/www/html/m152.vanloo.ch;

	index index.html;

	expires $expires;

	location ~ ^/images/.*\.(png|jpe?g)$ {
		add_header Vary Accept;
		try_files $uri$avif_suffix$webp_suffix $uri$avif_suffix
		$uri$webp_suffix $uri =404;
        }

	location ~* ^.+\.(?:css|cur|js|jpe?g|gif|htc|ico|png|html|xml|otf|ttf|eot|woff|woff2|svg|avif|webm|webp)$ {
	    access_log off;
	    expires 30d;
	    add_header Cache-Control public;

	    tcp_nodelay off;

	    ## Set the OS file cache.
	    open_file_cache max=3000 inactive=120s;
	    open_file_cache_valid 45s;
	    open_file_cache_min_uses 2;
	    open_file_cache_errors off;
	}

    listen [::]:443 ssl; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/m152.vanloo.ch/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/m152.vanloo.ch/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}
server {
    if ($host = m152.vanloo.ch) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


	listen 80;
	listen [::]:80;

	server_name m152.vanloo.ch;
    return 404; # managed by Certbot


}
