<?php

define("TRACKING_PATH_NAME", "../tracking-data/");
date_default_timezone_set("UTC");

$errors = array();

if (isset($_POST['visited']) && (trim($_POST['visited']) != '')) {
    $visited = $_POST['visited'];
} else {
    $errors[] = "Missing required parameter 'visited'";
}

if (count($errors) === 0) {
    $remote_address = $_SERVER["REMOTE_ADDR"];

    if (empty($remote_address)) {
        echo "Failed retrieving remote address";
        return;
    }

    $enc_ip = base64_encode($remote_address);

    $fd = fopen(TRACKING_PATH_NAME.$enc_ip.".txt", "w+");

    if ($fd) {
        fwrite($fd, "---");
        fwrite($fd, date(DATE_ISO8601));
        fwrite($fd, "\n");
        fwrite($fd, $visited);
        fwrite($fd, "\n");

        fflush($fd);
        fclose($fd);
    } else {
        echo "Error opening file";
        return;
    }
} else {
    echo implode(", ", $errors);
}

echo "All done.";

?>
