function shouldShowCookie() {
    const acceptedCookie = document.cookie
        .split("; ")
        .find(row => row.startsWith("accepted"))
        ?.split('=')[1];

    return acceptedCookie ? false : true;
}

function showCookie() {
    const banner = document.querySelector(".cookie-banner");
    banner.style.display = "flex";
}

function acceptCookie() {
    let now = new Date();
    let time = now.getTime();
    // About a month.
    let expires = time + 1000 * 60 * 60 * 24 * 30;
    now.setTime(expires);

    document.cookie = "accepted=true;" + "expires=" + now.toUTCString() + ";path=/";

    hideCookie();
}

function hideCookie() {
    const banner = document.querySelector(".cookie-banner");
    banner.style.display = "none";
}

// Entry point
if (shouldShowCookie()) {
    showCookie();

    window.addEventListener("keydown", function(e) {
        const c = String.fromCharCode(e.which).toLowerCase();
        if (c === "a") {
            acceptCookie();
        } else if (c === "r") {
            hideCookie();
        }
    });
}
