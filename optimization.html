<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="author" content="Colin van Loo" />
    <meta name="description" content="Leistungsbeurteilung fuers Modul 152" />
    <meta name="og:image" content="" />
    <meta name="og:description" content="Leistungsbeurteilung fuers Modul 152" />
    <meta name="og:title" content="M152 - Multimedia-Inhalte in Webauftritt integrieren" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <base target="_self" />
    <link rel="stylesheet" href="styles.css" />
    <link rel="stylesheet" href="collapsible.css" />
    <link rel="icon" type="image/png" href="favicon.png" />
    <title>M152 - Multimedia-Inhalte in Webauftritt integrieren</title>
</head>

<body>
    <nav>
        <table>
            <tbody>
                <tr>
                    <td><a id="link-prev" href="k6.html">🠔 Previous</a></td>
                    <td><a href="index.html">Contents</a></td>
                    <td><a id="link-next" href="bonus.html">Next 🠖</a></td>
                </tr>
            </tbody>
        </table>
        <div id="scroll-progress"></div>
    </nav>
    <div id="modal" class="modal">
        <span id="modal-close" class="modal-close">&times;</span>
        <img id="modal-image" class="modal-image" />
        <div id="modal-caption" class="modal-caption"></div>
    </div>
    <main>
        <div class="page">
            <div class="content">
                <h1>Optimization</h1>
                <h1 class="idx">
                    <a href="index.html">M152 - Multimedia</a>
                    <span class="section"><a href="k6.html">K6</a></span>
                </h1>
                <p>
                    We've already covered optimization in the previous section
                    about testing. Let's look at it now in some more detail.
                </p>
                <h2><a href="#no-javascript" name="no-javascript">No JavaScript</a></h2>
                <p>
                    Obviously, the less JavaScript (or any code in general) a
                    page has to execute, the faster it is going to run.
                </p>
                <p>
                    Before implementing anything in JavaScript, think
                    whether this is really necessary, and look for
                    simpler ways first.
                </p>
                <p>
                    For example, the side-notes on this page will break,
                    when a user has JavaScript disabled. There are good
                    options for JS-less side-notes. However, to
                    implement them, I would have needed to change quite
                    a bit about the structure of my page.
                </p>
                <p>
                    All the code also needs to be maintained. Generally,
                    it's good to write what you can yourself. Otherwise,
                    a library is needed. Frameworks are (most often) a
                    bad idea. They force a ton of constraints on you,
                    include much code you won't ever need, and slow your
                    page down a lot.
                </p>
                <h2><a href="#web-safe-fonts" name="web-safe-font">Web Safe Fonts</a></h2>
                <p>
                    As already discussed in the <a href="template.html#web-safe-fonts">
                        template</a> section, using system fonts doesn't
                    only make your page load much faster, but also has
                    more advantages.
                </p>
                <p>
                    It allows a user to choose their own font, instead
                    of needing them to put up with your terrible taste
                    in fonts.
                </p>
                <p>
                    If this is not possible, at least ship the font with
                    your page. Don't use Google's Font API. It's slower
                    and also hurts the visitor's privacy.
                </p>
                <h2><a href="#images" name="images">Images</a></h2>
                <p>
                    As we <a href="multi-images.html">found out</a>,
                    AVIF and WebP are the smallest formats with the
                    highest quality.
                </p>
                <p>
                    Whenever possible, we want to serve AVIF. However,
                    WebP is more widely supported. One way to fall back
                    to WebP is by using the <code>picture</code>
                    element.
                </p>
                <pre>
<code>
<span class="line-number">&lt;picture&gt;</span>
<span class="line-number">    &lt;source srcset=&quot;kittens.avif&quot;&gt;</span>
<span class="line-number">    &lt;source srcset=&quot;kittens.webp&quot;&gt;</span>
<span class="line-number">    &lt;img src=&quot;kittens.png&quot; alt=&quot;A picture of kittens.&quot; title=&quot;Kittens&quot; /&gt;</span>
<span class="line-number">&lt;/picture&gt;</span>
</code>
</pre>
                <p>
                    Another option is to configure it at the webserver
                    (NGINX) level, using <code>Accept</code> headers.
                    This practice is known as <em>content
                        negotiation</em>.
                </p>
                <pre>
<code>
<span class="line-number">http {</span>
<span class="line-number">    map $http_accept $webp_suffix {</span>
<span class="line-number">        default       &quot;&quot;;</span>
<span class="line-number">        &quot;~image/webp&quot; &quot;.webp&quot;;</span>
<span class="line-number">    }</span>
<span class="line-number">    map $http_accept $avif_suffix {</span>
<span class="line-number">        default       &quot;&quot;;</span>
<span class="line-number">        &quot;~image/avif&quot; &quot;.avif&quot;;</span>
<span class="line-number">    }</span>
<span class="line-number">}</span>

<span class="line-number">server {</span>
<span class="line-number">    location ~ ^/images/.*\.(png|jpe?g)$ {</span>
<span class="line-number">        add_header Vary Accept;</span>
<span class="line-number">        try_files $uri$avif_suffix$webp_suffix $uri$avif_suffix</span>
<span class="line-number">        $uri$webp_suffix $uri =404;</span>
<span class="line-number">    }</span>
<span class="line-number">}</span>
</code>
</pre>
                <p>
                    For more information, see <a href="https://vincent.bernat.ch/en/blog/2021-webp-avif-nginx" target="_blank">vincent.bernat.ch, Serving WebP & AVIF
                        images with Nginx.</a>
                </p>
                <h2><a href="#cache" name="cache">Cache</a></h2>
                <p>
                    Using cache policies, we can instruct user agents to keep
                    around files for longer in their caches. Our font files are
                    unlikely to change much. Telling the UA to keep them cached
                    for maybe a month, can speed up page loading by saving us
                    from doing unnecessary requests.
                </p>
                <p>
                    To do this, we need to configure our web server to
                    send <code>Cache-Control</code> headers in its
                    response. Those look similar to this:
                    <code>Cache-Control: max-age=31536000</code> and
                    include the maximum time a static resource is
                    allowed to be cached.
                </p>
                <p>
                    An example for NGINX might look like this:
                </p>
                <pre>
<code>
<span class="line-number">## All static files will be served directly.</span>
<span class="line-number">location ~* ^.+\.(?:css|cur|js|jpe?g|gif|htc|ico|png|html|xml|otf|ttf|eot|woff|woff2|svg|webp|webm|avif)$ {</span>
<span class="line-number">    access_log off;</span>
<span class="line-number">    expires 30d;</span>
<span class="line-number">    add_header Cache-Control public;</span>

<span class="line-number">    tcp_nodelay off;</span>

<span class="line-number">    ## Set the OS file cache.</span>
<span class="line-number">    open_file_cache max=3000 inactive=120s;</span>
<span class="line-number">    open_file_cache_valid 45s;</span>
<span class="line-number">    open_file_cache_min_uses 2;</span>
<span class="line-number">    open_file_cache_errors off;</span>
<span class="line-number">}</span>
</code>
</pre>
                <h2><a href="#results" name="results">Results</a></h2>
                <p>
                    But did the above improvements work? Absolutely!
                </p>
                <p>
                    PageSpeed's insights give us a 100 points (mobile and
                    desktop), and this is on the by far worst page on this
                    entire site: the <span name="worst-page">images</span>
                    page.
                </p>
                <aside name="worst-page">
                    <p>
                        It is the worst page, because the image format
                        optimization does not work for it. The page compares
                        images, and therefore serves many terrible formats
                        (such as BMP).
                    </p>
                    <p>
                        So the 100 points are actually more thanks to a fast
                        network connection to the server, and the efficient
                        cache policy.
                    </p>
                </aside>
                <img src="images/pagespeed-images-mobile-100.png" alt="Screenshot of PageSpeed Insights giving the images page 100 points on mobile." title="PageSpeed results for mobile" />
                <img src="images/pagespeed-images-desktop-100.png" alt="Screenshot of PageSpeed Insights giving the images page 100 points on desktop." title="PageSpeed results for desktop" />
                <p>
                    Let's also look at the image format trickery in action.
                </p>
                <p>
                    Internet Explorer can't display either of the two modern
                    formats. I'm not sure if it even sends an
                    <code>Accept</code> header. As a result, it is served a
                    dusty, old, and large PNG.
                </p>
                <img src="images/its-a-png.png" alt="Screenshot of IE, showing how the downloaded file is a PNG." title="It's a PNG!" />
                <p>
                    Edge, a bit of a more modern browser, still can't support
                    AVIF, but very well WebP. So that's exactly what it gets.
                    Notice how the path in the URL bar lies &ldquo;PNG&rdquo;,
                    but the file explorer correctly shows &ldquo;WebP&rdquo;.
                </p>
                <img src="images/its-a-webp.png" alt="Screenshot of Edge, showing the downloaded file is a WebP." title="It's a WebP!" />
                <p>
                    Finally, Firefox, the coolest of the bunch, renders AVIF
                    with ease.
                </p>
                <img src="images/its-an-avif.png" alt="Screenshot of Firefox, showing the downloaded file is an AVIF." title="It's an AVIF!" />
            </div>
        </div>
    </main>
    <footer>© 2022 <a href="mailto:webmaster@vanloo.ch">Colin van Loo</a></footer>
    <script src="collapsible.js"></script>
    <script src="script.js"></script>
    <script src="scroll-progress.js"></script>
    <script src="tracking.js"></script>
    <script src="modal.js"></script>
    <script>
        registerAllInMain();
    </script>
</body>

</html>
